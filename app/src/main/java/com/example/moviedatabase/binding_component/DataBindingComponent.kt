package com.example.moviedatabase.binding_component

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.moviedatabase.R

object DataBindingComponent {
    private const val URL = "https://image.tmdb.org/t/p/w500"

    @JvmStatic
    @BindingAdapter("setResource")
    fun setImage(imageView: ImageView, posterPath: String?) {

        if (posterPath != null) {
            Glide.with(imageView)
                .load(URL + posterPath)
                .into(imageView)
        }
        else {
            imageView.setImageResource(R.mipmap.image_placeholder)
        }
    }
}