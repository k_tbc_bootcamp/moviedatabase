package com.example.moviedatabase.http_requests

import com.example.moviedatabase.custom_interfaces.CustomCallback
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object DataLoader {

    const val API_KEY = "f0491ffda0b4daceadd878381a40159f"

    private const val ERROR = "status_message"
    private const val BASE_URL = "https://api.themoviedb.org/3/"

    //https://api.themoviedb.org/3/search/movie?api_key=f0491ffda0b4daceadd878381a40159f&query=ylemmuteli

    private const val HTTP_200_OK = 200
    private const val HTTP_201_CREATED = 201
    private const val HTTP_204_NO_CONTENT = 204
    private const val HTTP_400_BAD_REQUEST = 400
    private const val HTTP_401_UNAUTHORIZED = 401
    private const val HTTP_404_NOT_FOUND = 404
    private const val HTTP_500_INTERNAL_SERVER_ERROR = 500


    private var retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(
        ApiService::class.java
    )

    //genres
    fun getRequest(apiKey: String, callback: CustomCallback) {
        val call = service.getRequest(apiKey)
        call.enqueue(
            onCallback(
                callback
            )
        )
    }

    //genre movies
    fun getRequest(apiKey: String, genre: Int, callback: CustomCallback) {
        val call = service.getRequest(apiKey, genre)
        call.enqueue(
            onCallback(
                callback
            )
        )
    }

    //movie
    fun getRequest(movieId: Int, apiKey: String, callback: CustomCallback) {
        val call = service.getRequest(movieId, apiKey)
        call.enqueue(
            onCallback(
                callback
            )
        )
    }

    //movies
    fun getRequest(apiKey: String, searchMovie: String, callback: CustomCallback) {
        val call = service.getRequest(apiKey, searchMovie)
        call.enqueue(onCallback(callback))
    }

    private fun onCallback(callback: CustomCallback) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            val statusCode = response.code()

            if (statusCode == HTTP_200_OK || statusCode == HTTP_201_CREATED) {
                callback.onResponse(response.body().toString())
            } else if (statusCode == HTTP_204_NO_CONTENT) {
                callback.onFailure("No content found")
            } else {
                try {
                    val errorJson = JSONObject(response.errorBody()!!.string())
                    var errorString: String? = null

                    if (errorJson.has(ERROR)) {
                        errorString = errorJson.getString(ERROR)
                    }

                    if (statusCode == HTTP_400_BAD_REQUEST || statusCode == HTTP_401_UNAUTHORIZED ||
                        statusCode == HTTP_404_NOT_FOUND
                    ) {
                        callback.onFailure(errorString)
                    } else if (statusCode == HTTP_500_INTERNAL_SERVER_ERROR) {
                        callback.onFailure(errorString)
                    }
                } catch (e: JSONException) {
                    callback.onFailure(e.message.toString())
                }
            }
        }

    }

    interface ApiService {
        @GET("genre/movie/list")
        fun getRequest(@Query("api_key") apiKey: String): Call<String>

        @GET("discover/movie")
        fun getRequest(
            @Query("api_key") apiKey: String,
            @Query("with_genres") genre: Int
        ): Call<String>

        @GET("movie/{movieId}")
        fun getRequest(
            @Path("movieId") movieId: Int,
            @Query("api_key") apiKey: String
        ): Call<String>

        @GET("search/movie")
        fun getRequest(
            @Query("api_key") apiKey: String,
            @Query("query") searchMovie: String
        ): Call<String>
    }
}