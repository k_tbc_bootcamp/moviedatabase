package com.example.moviedatabase.shared_preferences

import android.content.Context
import android.content.SharedPreferences
import com.example.moviedatabase.application.App.Companion.context

class UserData private constructor() {
    companion object {
        const val EMAIL = "email"
        var userDataInstance: UserData? = null

        fun getInstance(): UserData {
            if (userDataInstance == null) {
                userDataInstance = UserData()
            }
            return userDataInstance!!
        }
    }

    private val sharedPreferences: SharedPreferences by lazy {
        context!!.getSharedPreferences("userData", Context.MODE_PRIVATE)
    }

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    fun saveData(key: String, value: String) {
        editor.putString(key, value)
        editor.apply()
    }

    fun getData(key: String): String {
        return sharedPreferences.getString(key, "")!!
    }

    fun removeData(key: String) {
        if (sharedPreferences.contains(key)) {
            editor.remove(key)
            editor.apply()
        }
    }
}