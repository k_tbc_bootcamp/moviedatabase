package com.example.moviedatabase.custom_interfaces

interface CustomOnClick {
    fun onClick(adapterPosition: Int)
}