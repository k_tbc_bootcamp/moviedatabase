package com.example.moviedatabase.custom_interfaces

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}