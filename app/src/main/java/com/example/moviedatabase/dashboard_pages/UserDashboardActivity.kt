package com.example.moviedatabase.dashboard_pages

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.moviedatabase.R
import com.example.moviedatabase.dashboard_pages.ui.genres.GenresFragment
import com.example.moviedatabase.dashboard_pages.ui.profile.ProfileFragment
import com.example.moviedatabase.dashboard_pages.ui.search.SearchFragment
import kotlinx.android.synthetic.main.activity_user_dashboard.*

class UserDashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_dashboard)

        init()
    }

    private fun init() {
        val fragments = mutableListOf<Fragment>()
        fragments.add(GenresFragment())
        fragments.add(SearchFragment())
        fragments.add(ProfileFragment(intent.getStringExtra("email")!!))

        nav_view_pager.adapter = FragmentViewPagerAdapter(supportFragmentManager, fragments)

        nav_view_pager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                nav_view.menu.getItem(position).isChecked = true
            }
        })

        nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_genres -> nav_view_pager.currentItem = 0
                R.id.navigation_search -> nav_view_pager.currentItem = 1
                R.id.navigation_profile -> nav_view_pager.currentItem = 2
            }
            true
        }
    }
}
