package com.example.moviedatabase.dashboard_pages.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.moviedatabase.R
import com.example.moviedatabase.authentication_page.activities.AuthenticationActivity
import com.example.moviedatabase.shared_preferences.UserData
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment(private val email: String) : Fragment() {

    private lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_profile, container, false)
        itemView.emailTextView.text = email
        itemView.logOutButton.setOnClickListener {
            UserData.getInstance().removeData(UserData.EMAIL)

            val intent = Intent(context, AuthenticationActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            startActivity(intent)
        }
        return itemView
    }

}
