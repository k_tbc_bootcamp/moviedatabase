package com.example.moviedatabase.dashboard_pages.ui.genres

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedatabase.R
import com.example.moviedatabase.base_fragment.BaseFragment
import com.example.moviedatabase.custom_interfaces.CustomOnClick
import com.example.moviedatabase.genre_movies_pages.GenreMoviesActivity
import kotlinx.android.synthetic.main.fragment_genres.*

class GenresFragment : BaseFragment() {

    private lateinit var genresModelView: GenresModelView
    private lateinit var genresAdapter: GenresRecyclerViewAdapter

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        genresModelView = viewModel as GenresModelView
        setObservers()
        genresModelView.initializeGenres()
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_genres
    }

    override fun getViewModelClass(): ViewModel {
        return GenresModelView()
    }

    private fun setObservers() {

        //onFailure toast
        genresModelView.returnError().observe(viewLifecycleOwner, Observer {
            val error = genresModelView.returnError().value
            Toast.makeText(context, error, Toast.LENGTH_LONG).show()
        })

        //onResponse
        genresModelView.returnGenres().observe(viewLifecycleOwner, Observer {
            val genres = genresModelView.returnGenres().value

            genreRecyclerView.layoutManager = LinearLayoutManager(context)
            genresAdapter = GenresRecyclerViewAdapter(genres!!, object: CustomOnClick {
                override fun onClick(adapterPosition: Int) {
                    val genre = genres[adapterPosition]

                    val intent = Intent(context, GenreMoviesActivity::class.java).apply {
                        putExtra("genreId", genre.id)
                    }
                    startActivity(intent)
                }
            })
            genreRecyclerView.adapter = genresAdapter
        })
    }
}
