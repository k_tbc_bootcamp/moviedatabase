package com.example.moviedatabase.dashboard_pages.ui.genres

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviedatabase.custom_interfaces.CustomCallback
import com.example.moviedatabase.http_requests.DataLoader
import com.google.gson.Gson
import org.json.JSONObject

class GenresModelView : ViewModel() {

    private val _genres = MutableLiveData<MutableList<GenreModel>>()
    private val _error = MutableLiveData<String>()

    fun returnError(): LiveData<String> {
        return _error
    }

    fun setError(error: String?) {
        _error.value = error
    }

    fun setGenres(genres: MutableList<GenreModel>) {
        _genres.value = genres
    }

    fun returnGenres(): LiveData<MutableList<GenreModel>> {
        return _genres
    }

    fun initializeGenres() {

        DataLoader.getRequest(
            DataLoader.API_KEY, object :
                CustomCallback {

                //failure
                override fun onFailure(error: String?) {
                    setError(error)
                }

                //success
                override fun onResponse(response: String) {
                    val responseJsonObject = JSONObject(response)

                    if (responseJsonObject.has("genres")) {
                        val genresJsonArray = responseJsonObject.getJSONArray("genres")
                        val tempArray = mutableListOf<GenreModel>()

                        (0 until genresJsonArray.length()).forEach {
                            val genreJsonObject = genresJsonArray.get(it) as JSONObject
                            val genre =
                                Gson().fromJson(genreJsonObject.toString(), GenreModel::class.java)
                            tempArray.add(genre)
                        }

                        setGenres(tempArray)
                    }
                }
            })
    }

}