package com.example.moviedatabase.dashboard_pages.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.moviedatabase.R
import com.example.moviedatabase.movies_pages.MoviesActivity
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.*

class SearchFragment : Fragment() {

    private var itemView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_search, container, false)
        return itemView
    }

    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
        itemView.searchMovieButton.setOnClickListener {
            val searchMovie = searchMovieEditText.text.toString()

            if (searchMovie.isNotEmpty()) {
                val intent = Intent(context, MoviesActivity::class.java).apply {
                    putExtra("title", searchMovie)
                }
                startActivity(intent)
            }
            else {
                Toast.makeText(context, "Please fill in the field.", Toast.LENGTH_LONG).show()
            }
        }
    }

}
