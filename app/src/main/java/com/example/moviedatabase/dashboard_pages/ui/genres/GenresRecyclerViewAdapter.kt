package com.example.moviedatabase.dashboard_pages.ui.genres

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.moviedatabase.custom_interfaces.CustomOnClick
import com.example.moviedatabase.R
import com.example.moviedatabase.databinding.GenresRecyclerViewItemBinding

class GenresRecyclerViewAdapter(
    private val genres: MutableList<GenreModel>,
    private val customOnClick: CustomOnClick
) :
    RecyclerView.Adapter<GenresRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: GenresRecyclerViewItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.genres_recycler_view_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return genres.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(private val binding: GenresRecyclerViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.genreCardView.setOnClickListener {
                customOnClick.onClick(adapterPosition)
            }
            binding.genreModel = genres[adapterPosition]
        }
    }
}