package com.example.moviedatabase.authentication_page

import androidx.lifecycle.*

class AuthenticationViewModel: ViewModel() {

    private val _user: MutableLiveData<UserModel> = MutableLiveData()
    private val _isChecked: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }

    fun setUser(user: UserModel) {
        _user.value = user
    }

    fun setToggled(isChecked: Boolean) {
        _isChecked.value = isChecked
    }

    fun returnUser(): LiveData<UserModel> {
        return _user
    }

    fun returnToggled(): LiveData<Boolean> {
        return _isChecked
    }

}