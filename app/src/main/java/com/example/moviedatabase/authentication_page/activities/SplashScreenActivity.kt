package com.example.moviedatabase.authentication_page.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.moviedatabase.R
import com.example.moviedatabase.dashboard_pages.UserDashboardActivity
import com.example.moviedatabase.shared_preferences.UserData

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        init()
    }

    private fun init() {
        handler = Handler()
    }

    override fun onStart() {
        super.onStart()
        handler.postDelayed(runnable, 2000)
    }

    private val runnable = Runnable {

        val email = UserData.getInstance().getData(UserData.EMAIL)
        if (email != "") {
            val intent = Intent(this, UserDashboardActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                putExtra("email", email)
            }
            startActivity(intent)
        }
        else {
            val intent = Intent(this, AuthenticationActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            }
            startActivity(intent)
        }


    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }
}
