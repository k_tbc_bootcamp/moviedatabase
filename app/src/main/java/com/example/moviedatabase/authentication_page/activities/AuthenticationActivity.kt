package com.example.moviedatabase.authentication_page.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.moviedatabase.authentication_page.AuthenticationViewModel
import com.example.moviedatabase.R
import com.example.moviedatabase.authentication_page.UserModel
import com.example.moviedatabase.dashboard_pages.UserDashboardActivity
import com.example.moviedatabase.shared_preferences.UserData
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var authenticationViewModel: AuthenticationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_authentication)

        init()
        setListeners()
        setObservers()
    }

    private fun init() {
        authenticationViewModel = ViewModelProvider(this).get(AuthenticationViewModel::class.java)
    }

    private fun setListeners() {
        //switch listener
        registerSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            authenticationViewModel.setToggled(isChecked)
        }

        //logIn button
        logInButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                authenticationViewModel.setUser(UserModel(email, password))
            } else {
                Toast.makeText(this, "Please fill in all the fields.", Toast.LENGTH_SHORT).show()
            }
        }

        //register button
        registerButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            val repeatedPassword = repeatPasswordEditText.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty() && repeatedPassword.isNotEmpty()) {
                if (password == repeatedPassword) {
                    authenticationViewModel.setUser(UserModel(email, password))
                } else {
                    Toast.makeText(this, "The passwords don't match.", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "Please fill in all the fields.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setObservers() {
        //switch observer
        authenticationViewModel.returnToggled().observe(this, Observer {
            if (it) {
                headerTextView.text = getString(R.string.register)
                logInButton.visibility = View.GONE
                repeatPasswordTextView.visibility = View.VISIBLE
                repeatPasswordEditText.visibility = View.VISIBLE
                registerButton.visibility = View.VISIBLE
            } else {
                headerTextView.text = getString(R.string.log_in)
                registerButton.visibility = View.GONE
                repeatPasswordEditText.visibility = View.GONE
                repeatPasswordTextView.visibility = View.GONE
                logInButton.visibility = View.VISIBLE
            }
        })

        //user observer
        authenticationViewModel.returnUser().observe(this, Observer {
            auth = FirebaseAuth.getInstance()
            val isChecked = authenticationViewModel.returnToggled().value!!
            val user = authenticationViewModel.returnUser().value!!

            //registration
            if (isChecked) {
                auth.createUserWithEmailAndPassword(user.email, user.password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            UserData.getInstance().saveData(UserData.EMAIL, user.email)
                            goToUserDashboardActivity(user)
                        } else {
                            Toast.makeText(
                                this, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }

            //login
            else {
                auth.signInWithEmailAndPassword(user.email, user.password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            UserData.getInstance().saveData(UserData.EMAIL, user.email)
                            goToUserDashboardActivity(user)
                        } else {
                            Toast.makeText(
                                this, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        })
    }

    private fun goToUserDashboardActivity(user: UserModel) {
        val intent = Intent(this, UserDashboardActivity::class.java).apply {
            putExtra("email", user.email)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        startActivity(intent)
    }
}
