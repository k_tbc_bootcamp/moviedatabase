package com.example.moviedatabase.movies_pages.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.example.moviedatabase.R
import com.example.moviedatabase.databinding.FragmentMovieBinding
import com.example.moviedatabase.movies_pages.view_models.MovieFragmentViewModel

class MovieFragment(private val movieId: Int) : Fragment() {

    private var itemView: View? = null
    private lateinit var movieFragmentViewModel: MovieFragmentViewModel
    private var binding: FragmentMovieBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (itemView == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie, container, false)
            itemView = binding!!.root

            movieFragmentViewModel = ViewModelProvider(this).get(MovieFragmentViewModel::class.java)
            movieFragmentViewModel.movieId = movieId
            setObservers()
            movieFragmentViewModel.initializeMovie()
        }
        return itemView
    }

    private fun setObservers() {
        movieFragmentViewModel.returnError().observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })

        movieFragmentViewModel.returnMovie().observe(viewLifecycleOwner, Observer {
            binding!!.movieModel = it
        })
    }

}
