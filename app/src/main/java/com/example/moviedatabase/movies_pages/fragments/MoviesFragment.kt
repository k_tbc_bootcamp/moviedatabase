package com.example.moviedatabase.movies_pages.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedatabase.R
import com.example.moviedatabase.movies_pages.MoviesRecyclerViewAdapter
import com.example.moviedatabase.custom_interfaces.CustomOnClick
import com.example.moviedatabase.movies_pages.view_models.MoviesFragmentViewModel
import kotlinx.android.synthetic.main.fragment_movies.view.*

class MoviesFragment(private val title: String) : Fragment() {

    private lateinit var moviesFragmentViewModel: MoviesFragmentViewModel
    private var moviesAdapter: MoviesRecyclerViewAdapter? = null
    var itemView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.fragment_movies, container, false)
            moviesFragmentViewModel =
                ViewModelProvider(this).get(MoviesFragmentViewModel::class.java)
            moviesFragmentViewModel.title = title
            setObservers()
            moviesFragmentViewModel.initializeMoviesOverview()
        }
        return itemView
    }

    private fun setObservers() {
        moviesFragmentViewModel.returnError().observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })

        moviesFragmentViewModel.returnMoviesOverview().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                itemView!!.moviesRecyclerView.layoutManager = LinearLayoutManager(context)
                moviesAdapter =
                    MoviesRecyclerViewAdapter(
                        it,
                        object : CustomOnClick {
                            override fun onClick(adapterPosition: Int) {
                                activity!!.supportFragmentManager.beginTransaction()
                                    .replace(
                                        R.id.moviesFragment,
                                        MovieFragment(
                                            it[adapterPosition].id
                                        )
                                    )
                                    .addToBackStack("movieFragment").commit()
                            }

                        })
                itemView!!.moviesRecyclerView.adapter = moviesAdapter
            }
            else {
                itemView!!.moviesRecyclerView.visibility = View.GONE
                itemView!!.notFoundTextView.visibility = View.VISIBLE
            }
        })
    }

    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {

    }

}
