package com.example.moviedatabase.movies_pages.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviedatabase.custom_interfaces.CustomCallback
import com.example.moviedatabase.genre_movies_pages.models.MovieOverviewModel
import com.example.moviedatabase.http_requests.DataLoader
import com.google.gson.Gson
import org.json.JSONObject

class MoviesFragmentViewModel: ViewModel() {

    var title = ""

    private val _moviesOverview = MutableLiveData<MutableList<MovieOverviewModel>>()
    private val _error = MutableLiveData<String>()

    fun setMoviesOverview(moviesOverview: MutableList<MovieOverviewModel>) {
        _moviesOverview.value = moviesOverview
    }

    fun returnMoviesOverview(): LiveData<MutableList<MovieOverviewModel>> {
        return _moviesOverview
    }

    fun setError(error: String?) {
        _error.value = error
    }

    fun returnError(): LiveData<String> {
        return _error
    }

    fun initializeMoviesOverview() {
        DataLoader.getRequest(DataLoader.API_KEY, title, object: CustomCallback {
            override fun onFailure(error: String?) {
                setError(error)
            }

            override fun onResponse(response: String) {
                val responseJsonObject = JSONObject(response)

                if (responseJsonObject.has("results")) {
                    val resultsJsonArray = responseJsonObject.getJSONArray("results")

                    val tempArray = mutableListOf<MovieOverviewModel>()
                    (0 until resultsJsonArray.length()).forEach {
                        val movieOverviewJsonObject = resultsJsonArray.get(it) as JSONObject
                        val movieOverview = Gson().fromJson(movieOverviewJsonObject.toString(), MovieOverviewModel::class.java)

                        tempArray.add(movieOverview)
                    }

                    setMoviesOverview(tempArray)
                }
            }

        })
    }
}