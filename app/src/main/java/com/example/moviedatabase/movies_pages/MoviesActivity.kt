package com.example.moviedatabase.movies_pages

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.moviedatabase.R
import com.example.moviedatabase.movies_pages.fragments.MoviesFragment

class MoviesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)

        init()
    }

    private fun init() {
        val title = intent.getStringExtra("title")!!
        supportFragmentManager.beginTransaction().add(R.id.moviesFragment, MoviesFragment(title)).commit()

    }
}
