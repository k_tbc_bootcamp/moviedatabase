package com.example.moviedatabase.movies_pages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.moviedatabase.custom_interfaces.CustomOnClick
import com.example.moviedatabase.R
import com.example.moviedatabase.databinding.MoviesRecyclerViewItemBinding
import com.example.moviedatabase.genre_movies_pages.models.MovieOverviewModel

class MoviesRecyclerViewAdapter(
    private val moviesOverview: MutableList<MovieOverviewModel>,
    private val customOnClick: CustomOnClick
) :
    RecyclerView.Adapter<MoviesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MoviesRecyclerViewItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.movies_recycler_view_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return moviesOverview.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(private val binding: MoviesRecyclerViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.moviesCardView.setOnClickListener {
                customOnClick.onClick(adapterPosition)
            }
            binding.movieOverviewModel = moviesOverview[adapterPosition]
        }
    }
}