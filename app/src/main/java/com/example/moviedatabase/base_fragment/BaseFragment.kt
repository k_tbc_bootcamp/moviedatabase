package com.example.moviedatabase.base_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

abstract class BaseFragment: Fragment() {

    var itemView: View? = null
    lateinit var viewModel: ViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (itemView == null) {
            viewModel = ViewModelProvider(this).get(getViewModelClass()::class.java)
            itemView = inflater.inflate(getLayoutResource(), container, false)
            start(inflater, container, savedInstanceState)
        }
        return itemView
    }

    abstract fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )

    abstract fun getLayoutResource(): Int

    abstract fun getViewModelClass(): ViewModel
}
