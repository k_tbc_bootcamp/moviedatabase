package com.example.moviedatabase.genre_movies_pages.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviedatabase.custom_interfaces.CustomCallback
import com.example.moviedatabase.genre_movies_pages.models.MovieModel
import com.example.moviedatabase.http_requests.DataLoader
import com.google.gson.Gson

class GenreMovieViewModel: ViewModel() {

    var movieId = 0

    private val _movie = MutableLiveData<MovieModel>()
    private val _error = MutableLiveData<String>()

    fun setError(error: String?) {
        _error.value = error
    }

    fun returnError(): LiveData<String> {
        return _error
    }

    fun setMovie(movie: MovieModel) {
        _movie.value = movie
    }

    fun returnMovie(): LiveData<MovieModel> {
        return _movie
    }

    fun initializeMovie() {
        DataLoader.getRequest(movieId, DataLoader.API_KEY, object: CustomCallback {

            //onFailure
            override fun onFailure(error: String?) {
                setError(error)
            }

            //onSuccess
            override fun onResponse(response: String) {
                val movie = Gson().fromJson(response, MovieModel::class.java)
                setMovie(movie)
            }

        })
    }
}