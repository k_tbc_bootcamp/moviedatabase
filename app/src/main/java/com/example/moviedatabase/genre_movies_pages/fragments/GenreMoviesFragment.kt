package com.example.moviedatabase.genre_movies_pages.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedatabase.R
import com.example.moviedatabase.genre_movies_pages.GenreMoviesRecyclerViewAdapter
import com.example.moviedatabase.custom_interfaces.CustomOnClick
import com.example.moviedatabase.base_fragment.BaseFragment
import com.example.moviedatabase.genre_movies_pages.view_models.GenreMoviesViewModel
import kotlinx.android.synthetic.main.fragment_genre_movies.*

class GenreMoviesFragment(private val genreId: Int) : BaseFragment() {

    lateinit var genreMoviesViewModel: GenreMoviesViewModel
    lateinit var genreMoviesAdapter: GenreMoviesRecyclerViewAdapter

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        genreMoviesViewModel = viewModel as GenreMoviesViewModel
        genreMoviesViewModel.genreId = genreId
        setObservers()
        genreMoviesViewModel.initializeGenreMovies()
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_genre_movies
    }

    override fun getViewModelClass(): ViewModel {
        return GenreMoviesViewModel()
    }

    private fun setObservers() {

        //onFailure toast
        genreMoviesViewModel.returnError().observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, genreMoviesViewModel.returnError().value, Toast.LENGTH_LONG)
                .show()
        })

        //onResponse
        genreMoviesViewModel.returnGenreMovies().observe(viewLifecycleOwner, Observer {
            genreMoviesRecyclerView.layoutManager = LinearLayoutManager(context)
            genreMoviesAdapter =
                GenreMoviesRecyclerViewAdapter(it, object : CustomOnClick {
                    override fun onClick(adapterPosition: Int) {
                        activity!!.supportFragmentManager.beginTransaction()
                            .replace(R.id.genreMoviesFragment,
                                GenreMovieFragment(
                                    it[adapterPosition].id
                                )
                            )
                            .addToBackStack("genreMovieFragment").commit()
                    }
                })
            genreMoviesRecyclerView.adapter = genreMoviesAdapter
        })
    }
}
