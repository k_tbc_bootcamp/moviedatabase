package com.example.moviedatabase.genre_movies_pages.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


class MovieOverviewModel() : Parcelable {
    var id = 0
    @SerializedName("poster_path")
    var posterPath = ""
    @SerializedName("original_title")
    var originalTitle = ""
    var overview = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        posterPath = parcel.readString()!!
        originalTitle = parcel.readString()!!
        overview = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(posterPath)
        parcel.writeString(originalTitle)
        parcel.writeString(overview)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MovieOverviewModel> {
        override fun createFromParcel(parcel: Parcel): MovieOverviewModel {
            return MovieOverviewModel(
                parcel
            )
        }

        override fun newArray(size: Int): Array<MovieOverviewModel?> {
            return arrayOfNulls(size)
        }
    }
}