package com.example.moviedatabase.genre_movies_pages.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviedatabase.custom_interfaces.CustomCallback
import com.example.moviedatabase.genre_movies_pages.models.MovieOverviewModel
import com.example.moviedatabase.http_requests.DataLoader
import com.google.gson.Gson
import org.json.JSONObject

class GenreMoviesViewModel : ViewModel() {

    var genreId = 0

    private val _genreMovies = MutableLiveData<MutableList<MovieOverviewModel>>()
    private val _error = MutableLiveData<String>()

    fun setError(error: String?) {
        _error.value = error
    }

    fun returnError(): LiveData<String> {
        return _error
    }

    fun setGenreMovies(movieOverviews: MutableList<MovieOverviewModel>) {
        _genreMovies.value = movieOverviews
    }

    fun returnGenreMovies(): LiveData<MutableList<MovieOverviewModel>> {
        return _genreMovies
    }

    fun initializeGenreMovies() {

        DataLoader.getRequest(DataLoader.API_KEY, genreId, object : CustomCallback {

            //onFailure
            override fun onFailure(error: String?) {
                setError(error)
            }

            //onSuccess
            override fun onResponse(response: String) {

                val responseJsonObject = JSONObject(response)

                if (responseJsonObject.has("results")) {
                    val genreMoviesJsonArray = responseJsonObject.getJSONArray("results")
                    val temp = mutableListOf<MovieOverviewModel>()

                    (0 until genreMoviesJsonArray.length()).forEach {
                        val genreMovieJsonObject = genreMoviesJsonArray.get(it) as JSONObject
                        val movieOverviewModel = Gson().fromJson(
                            genreMovieJsonObject.toString(),
                            MovieOverviewModel::class.java
                        )
                        temp.add(movieOverviewModel)
                    }
                    setGenreMovies(temp)
                }
            }

        })
    }
}