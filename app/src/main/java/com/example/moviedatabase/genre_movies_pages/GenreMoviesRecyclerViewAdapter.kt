package com.example.moviedatabase.genre_movies_pages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.moviedatabase.custom_interfaces.CustomOnClick
import com.example.moviedatabase.R
import com.example.moviedatabase.databinding.GenreMoviesRecyclerViewItemBinding
import com.example.moviedatabase.genre_movies_pages.models.MovieOverviewModel

class GenreMoviesRecyclerViewAdapter(
    private val genreMovies: MutableList<MovieOverviewModel>,
    private val customOnClick: CustomOnClick
) :
    RecyclerView.Adapter<GenreMoviesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: GenreMoviesRecyclerViewItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.genre_movies_recycler_view_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return genreMovies.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(private val binding: GenreMoviesRecyclerViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.genreMovieCardView.setOnClickListener {
                customOnClick.onClick(adapterPosition)
            }
            binding.movieOverviewModel = genreMovies[adapterPosition]
        }
    }
}