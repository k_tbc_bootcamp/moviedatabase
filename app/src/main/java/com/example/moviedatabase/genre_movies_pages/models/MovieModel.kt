package com.example.moviedatabase.genre_movies_pages.models

import com.google.gson.annotations.SerializedName

class MovieModel {
    @SerializedName("original_title")
    var title = ""

    @SerializedName("poster_path")
    var posterPath = ""

    var budget: Double = 0.0
    var homepage = ""
    var overview = ""

    @SerializedName("release_date")
    var releaseDate = ""

    @SerializedName("vote_average")
    var voteAverage: Double = 0.0
}