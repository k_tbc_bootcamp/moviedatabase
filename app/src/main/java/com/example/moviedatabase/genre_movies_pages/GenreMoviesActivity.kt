package com.example.moviedatabase.genre_movies_pages

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.moviedatabase.R
import com.example.moviedatabase.genre_movies_pages.fragments.GenreMoviesFragment

class GenreMoviesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genre_movies)

        init()
    }

    private fun init() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.genreMoviesFragment, GenreMoviesFragment(intent.getIntExtra("genreId", 0)))
            .commit()
    }
}
