package com.example.moviedatabase.genre_movies_pages.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.example.moviedatabase.R
import com.example.moviedatabase.databinding.FragmentGenreMovieBinding
import com.example.moviedatabase.genre_movies_pages.view_models.GenreMovieViewModel

class GenreMovieFragment(private val movieId: Int) : Fragment() {

    private lateinit var itemView: View
    private lateinit var genreMovieViewModel: GenreMovieViewModel
    private lateinit var binding: FragmentGenreMovieBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_genre_movie, container, false)
        genreMovieViewModel = ViewModelProvider(this).get(GenreMovieViewModel::class.java)
        genreMovieViewModel.movieId = movieId

        setObservers()
        genreMovieViewModel.initializeMovie()
        itemView = binding.root
        return itemView
    }

    private fun setObservers() {

        //onFailure toast
        genreMovieViewModel.returnError().observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })

        //onSuccess
        genreMovieViewModel.returnMovie().observe(viewLifecycleOwner, Observer {
            binding.movieModel = it
        })

    }

}
